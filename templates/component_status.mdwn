[[!templatebody <<ENDBODY
<table class="component_status">
<tr>
    <th colspan=2><TMPL_VAR name></th>
</tr>
<tr>
    <td valign=top align=right>Description:</td>
    <td valign=top>&nbsp;<TMPL_IF desc><TMPL_VAR desc><TMPL_ELSE>Unknown :(</TMPL_IF></td>
</tr>
<tr>
    <td valign=top align=right>Status:</td>
    <td valign=top>&nbsp;<TMPL_IF status><TMPL_VAR status><TMPL_ELSE>Unknown :(</TMPL_IF></td>
</tr>
<tr>
    <td valign=top align=right>Cost Estimate:</td>
    <td valign=top>&nbsp;<TMPL_IF cost><TMPL_VAR cost><TMPL_ELSE>Unknown :(</TMPL_IF></td>
</tr>
<tr>
    <td valign=top align=right>Plan:</td>
    <td valign=top>&nbsp;<TMPL_IF plan><TMPL_VAR plan><TMPL_ELSE>Unknown :(</TMPL_IF></td>
</tr>
<tr>
    <td valign=top align=right>Assigned to:</td>
    <td valign=top>&nbsp;<TMPL_IF who><TMPL_VAR who><TMPL_ELSE>Nobody :(</TMPL_IF></td>
</tr>

</table>
ENDBODY]]
