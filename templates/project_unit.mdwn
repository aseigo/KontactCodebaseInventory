[[!templatebody <<ENDBODY
# <TMPL_VAR name>
<table class="func_unit">
<tr>
    <td valign=top align=right>Plan</td>
    <td valign=top>&nbsp;<TMPL_IF plan><TMPL_VAR plan><TMPL_ELSE>Unknown :(</TMPL_IF></td>
</tr>
<TMPL_IF details>
<tr>
    <td valign=top align=right>Detailed analysis:</td>
    <td valign=top>&nbsp;[[<TMPL_VAR details>]]<tr>
</TMPL_IF>
<TMPL_IF complexity>
<tr>
    <td valign=top align=right>Complexity</td>
    <td valign=top>&nbsp;<TMPL_VAR complexity><tr>
</TMPL_IF>
<TMPL_IF effort>
<tr>
    <td valign=top align=right>Effort:</td>
    <td valign=top>&nbsp;<TMPL_VAR effort></td>
</tr>
</TMPL_IF>
</table>
ENDBODY]]
