[[!template name="korgac" id=component_status who="" cost="" status="" plan="Port" desc="Korganizer reminder daemon"]]

## General Notes

Very light usage of akonadi amongst its ~2kloc. Port, and consider perhaps how to do platform integration more effectively. Can be ported as used as-is for now. Can move to QML based UI later.

## Functional Units

[[!template id="func_unit" name="UI" complexity="2" effort="1"]]

The windows that get shown to the user when there is an alarm.

**Files**

* alarmdialog.cpp
    * The 'main' UI of the app, and nearly 50% of the codebase. Keep it as is for now, port away from Akonadi
* alarmdockwindow.cpp
    * The system tray item. Can perhaps be ditched and just left as a "headless" app

[[!template id="func_unit" name="DBus" complexity="1" effort="1"]]

* koalarmclient.cpp
    * a DBus interface; may as well be kept, though its actual value is perhaps arguable
* org.kde.korganizer.KOrgac.xml

[[!template id="func_unit" name="Misc" complexity="1" effort="1"]]

* korgacmain.cpp
    * main()
* mailclient.cpp
    * a copy of kdepimlibs/akonadi/calendar/mailclient_p.cpp
    * uses KCalCore, KCalUtils
    * uses Akonadi's message queuing, which needs a port
    * would be nice to offer a straight-forward Akonadi 2 mechanism for mail queueing


