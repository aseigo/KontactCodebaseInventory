[[!template name="kaddressbook" id=component_status who="cmollekopf" cost="" status="" plan="Port; earmark as usability target" desc="Addressbook UI"]]

## General Notes
* Printing could use an overhaul, but seems to be fairly standalone.
* Settings rely on KConfigXT
* Grantlee should be replaced by QML eventually
* Akonadi::ContactViewer needs to be refactored into a generic contact viewer not depending on akonadi
* Porting away from KABC should be trivial

## Functional Units

[[!template id="func_unit" name="xxport" complexity="1" effort="3"]]

Import/Export. Useful but needs an overhaul.
Uses a lot of dialogs in business logic

**Files**

**Deps**

* pimcommon
* KABC

[[!template id="func_unit" name="merge" complexity="1" effort="0"]]

Wizard to merge contacts. Largely disfunctional AFAIK.

**Files**

**Deps**

* pimcommon
* KABC

[[!template id="func_unit" name="printing" complexity="2" effort="1"]]

**Files**

**Deps**

* grantlee

[[!template id="func_unit" name="contactviewer" complexity="1" effort="1"]]

Single widget contact viewer.
All Grantlee code should be replaced with QML.

**Files**

**Deps**

* Akonadi::ContactViewer
* grantlee

[[!template id="func_unit" name="mainwindow" complexity="1" effort="4"]]

Three pane view, with folders, contacts, editor.

**Files**

**Deps**

[[!template id="func_unit" name="editor" complexity="3" effort="3"]]

Tabbed contact editor.

**Files**

**Deps**

* Akonadi::ContactEditorDialog
* akonadi/contact/editor/contacteditorwidget.h
