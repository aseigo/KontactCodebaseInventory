[[!template name="libkdepim" id=component_status who="cmollekopf" cost="" status="" plan="Port; earmark as usability target" desc="Addressbook UI"]]

## General Notes
* Collection of arbitrary functionality

## Functional Units

[[!template id="func_unit" name="freebusymodel"]]

A model that downloads freebusy information of attendees, and then populates the model with busy periods for individual attendees.
Used by the incidenceeditor for fb-scheduling.

**Files**

* freebusyitemmodel.cpp: 2-level model, toplevel attendees, second level freebusy periods. Used by ConflictResolver in IncidenceEditorNG.
* freebusyitem.cpp: handles downloading of fb-data. Used by freebusyitemmodel.
* freeperiodmodel.cpp: table model for free periods. Contains some domain logic to split free periods by days. FreePeriods are inserted by ConflictResolver. Used in list view in IncidenceEditorNG.
* freebusycalendar.cpp: populates a KCalCore::Calendar from a FreebusyItemModel

**Deps**

* KCalCore

[[!template id="func_unit" name="addressline" complexity="4" effort="2"]]

Addresseline that does autocompletion from baloo and ldap.

Pretty much all code is implemented in the UI. Some logic to extract to retrieve results from ldap,
and to organize results from different lookup sources. 2k lines of code!

There is a completionordereditor....

Used from IncidenceEditor for attendee completion and in kmail for to: completion.

[[!template id="func_unit" name="designer" complexity="0" effort="0"]]

Designerplugin for kdepim widgets.

[[!template id="func_unit" name="interfaces" complexity="0" effort="0"]]

DBus Interfaces.

[[!template id="func_unit" name="job" complexity="2" effort="0"]]

Akonadi specific jobs.

**Files**

* addcontactjob.cpp: add KABC:Contact if not already existing.
* addemailaddressjob.cpp: same as addcontactjob but with email address only, and a dialog to inform that contact is already existing.
* addemaildisplayjob.cpp: Create contact with dialog to select target collection
* collectionsearchjob.cpp: Search collections using baloo. Used in KOrganizer calendar selection.
* openemailaddressjob.cpp: Open a contact editor if contact is not already existing, based on email address.
* person.h/personsearchjob.cpp: Search for persons using abloo. Used in Korganizer calendar selection.

[[!template id="func_unit" name="ldap"]]

**Files**

* addhostdialog.cpp: KDialog that configures a KLDAP:LDAPServer
* kcmldap.cpp: KCModule to configure ldap server settings. Writes to config files.
* ldapclient.cpp: Client object to run queries against an ldap server
* ldapclientsearchconfig.cpp: Wrapper for config file. Loads/Stores KLDAP::LDAPServer
* ldapclientsearch.cpp: Another API to run LDAP queries
* ldapsearchdialog.cpp: KDialog to search contacts. Used in messagecomposer/recipient/recipientspicker.cpp and kaddressbook xxport.
* ldapqueryjob.cpp: Job that retrieves data specified by an ldap url
* ldapsession.cpp: QThread that runs ldapqueryjobs

[[!template id="func_unit" name="misc" complexity="1" effort="1"]]

**Files**

* broadcaststatus.cpp: Singleton to broadcast status messages that can be shown by various mainwindows in the statusline. Used in kmail, knode, akregator, and the summaryview. Drop.
* maillistdrag.cpp: Mail d&d that avoid unnecessary copying for in application d&d. Might be reusable.
* statisticsproxymodel.cpp: KIdentityProxyModel that exposes collection statistics. Drop.
* uistatesaver.cpp: Automatically discover qobject children of some types (splitter, ...), and save their state. Drop.

[[!template id="func_unit" name="multiplyingline" complexity="1" effort="1"]]

An editor that adds new lines as you type with checkboxes to select lines.
Used in incidenceeditor, attendeelist and mailcomposer receipienteditor.

Copy to incidenceeditor if we keep that, replace with something QML in the long run.
Almost entirely QWidget wrangling that should be repalced with a model.

**Deps**

* KDialog/KConfig/KLocale/...
* No Akonadi deps

[[!template id="func_unit" name="prefs" complexity="3" effort="0"]]

Migrate away from this.

KPrefsDialog: A framework to implement preference dialogs.
KPrefsModule used in kontact and korganizer, though mostly in korganizer in:

* korganizer/prefs/koprefsdialog.*
* korganizer/dialog/exportwebdialog.*

It contains a class hierarchy that represents various data types used in PIM app configuration UIs. The core is KPrefsWidget, KPrefsWidManager and KPrefsWid. ('Wid' appears to stand for 'Widgets'). The classes are used with KCModule/KPrefsDialog and KConfigSkeleton. The data types include:

* bool
* int
* time
* duration (a specialization of time)
* date
* color
* font
* enums (as a set of radio buttons, ergo KPrefsWidRadios)
* enums (as a combobox)
* strings
* paths

Each is implemented as its own class.

[[!template id="func_unit" name="progresswidget" complexity="1" effort="0"]]

Gathers progress from agents, and displays the progress widgets shown in kmail (I think). All UI or akonadi specific, not reusable.

[[!template id="func_unit" name="widgets" complexity="1" effort="0"]]

A bunch of widgets (date-picker, combobox, spellcheckedit, tag, ...), that we should be able to drop and replace by something QML.

