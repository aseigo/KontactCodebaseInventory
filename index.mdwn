# Kontact Codebase Inventory

This wiki is used by the [[Devel Team]] to build and maintain an overview of the existing Kontact codebase. This inventory contains porting notes, useful functionality and feature descriptions and refactoring effort estimates for components. The wiki is managed using ikiwiki with a shared git repository that can be found [[here|https://projects.kde.org/]].

Inventories:

* [[kdepim]]


All wikis are supposed to have a [[SandBox]], so this one does too.

